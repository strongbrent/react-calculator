# TDD React Course

[![pipeline status](https://gitlab.com/strongbrent/react-calculator/badges/master/pipeline.svg)](https://gitlab.com/strongbrent/react-calculator/commits/master)

This is my course work for: A simple Calculator app created for a blog post at [testdriven.io](https://testdriven.io/blog/tdd-with-react-jest-and-enzyme-part-one/).

## Local Setup

```sh
$ git clone git@github.com:calebpollman/react-calculator.git
```

```sh
$ cd react-calculator
```

```sh
$ npm install
```

## Run Locally

```sh
$ npm start
```

## Run Tests

```sh
$ npm test
```
